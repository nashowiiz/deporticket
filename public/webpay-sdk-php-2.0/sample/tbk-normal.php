<?php
/**
 * @author     Allware Ltda. (http://www.allware.cl)
 * @copyright  2015 Transbank S.A. (http://www.tranbank.cl)
 * @date       Jan 2015
 * @license    GNU LGPL
 * @version    2.0.1
 */

require_once( '../libwebpay/webpay.php' );
require_once( 'certificates/cert-normal.php' );

/** Configuracion parametros de la clase Webpay */
$sample_baseurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$configuration = new Configuration();
$configuration->setEnvironment($certificate['environment']);
$configuration->setCommerceCode($certificate['commerce_code']);
$configuration->setPrivateKey($certificate['private_key']);
$configuration->setPublicCert($certificate['public_cert']);
$configuration->setWebpayCert($certificate['webpay_cert']);

/** Creacion Objeto Webpay */
$webpay = new Webpay($configuration);

$action = isset($_GET["action"]) ? $_GET["action"] : 'init';

$post_array = false;

switch ($action) {

    default:

        $tx_step = "Init";

        /** Monto de la transacción */
        $amount = $_GET["TBK_MONTO"];

        /** Orden de compra de la tienda */
        $buyOrder = $_GET["TBK_ORDEN_COMPRA"];

        /** Código comercio de la tienda entregado por Transbank */
        $sessionId = $_GET["TBK_ID_SESION"];

        /** URL de retorno */
        $urlReturn = $sample_baseurl."?action=getResult";

        /** URL Final */
	$urlFinal  = $sample_baseurl."?action=end";

        $request = array(
            "amount"    => $amount,
            "buyOrder"  => $buyOrder,
            "sessionId" => $sessionId,
            "urlReturn" => $urlReturn,
            "urlFinal"  => $urlFinal,
        );

        /** Iniciamos Transaccion */
        $result = $webpay->getNormalTransaction()->initTransaction($amount, $buyOrder, $sessionId, $urlReturn, $urlFinal);
        var_dump($result);
        exit;
        /** Verificamos respuesta de inicio en webpay */
        if (!empty($result->token) && isset($result->token)) {
            $message = "Sesion iniciada con exito en Webpay";
            $token = $result->token;
            $next_page = $result->url;
        } else {
            $message = "webpay no disponible";
        }

        $button_name = "Continuar &raquo;";

        break;

    case "getResult":

        $tx_step = "Get Result";

        if (!isset($_POST["token_ws"]))
            break;

        /** Token de la transacción */
        $token = filter_input(INPUT_POST, 'token_ws');

        $request = array(
            "token" => filter_input(INPUT_POST, 'token_ws')
        );

        /** Rescatamos resultado y datos de la transaccion */
        $result = $webpay->getNormalTransaction()->getTransactionResult($token);

        // GUARDO EL REQUEST EN UN ARCHIVO CON IDENTIFICADOR DEL TOKEN
        $file = fopen("../../../storage/webpay/".$token."_webpay.log", "w");
        $linea="";
        foreach ($result as $key => $value) {
          if ($key =="detailOutput"){
            foreach ($result->detailOutput as $key2 => $value2) {
              $linea =$linea.$key2."=".$value2."&";
            }
          } elseif ($key =="cardDetail") {
            foreach ($result->cardDetail as $key2 => $value2) {
              $linea =$linea.$key2."=".$value2."&";
            }
          }else{
            $linea =$linea.$key."=".$value."&";
          }
        }
        fwrite($file , $linea);
        fclose($file);

        /** Verificamos resultado  de transacción */
        if ($result->detailOutput->responseCode === 0) {

            /** propiedad de HTML5 (web storage), que permite almacenar datos en nuestro navegador web */
            echo '<script>window.localStorage.clear();</script>';
            echo '<script>localStorage.setItem("authorizationCode", '.$result->detailOutput->authorizationCode.')</script>';
            echo '<script>localStorage.setItem("amount", '.$result->detailOutput->amount.')</script>';
            echo '<script>localStorage.setItem("buyOrder", '.$result->buyOrder.')</script>';

            $message = "Pago ACEPTADO por webpay (se deben guardatos para mostrar voucher)";
            $next_page = $result->urlRedirection;

        } else {
            $message = "Pago RECHAZADO por webpay - " . utf8_decode($result->detailOutput->responseDescription);
            $next_page = '';
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/WebpayFracaso/'.$token);
        }

        $button_name = "Continuar &raquo;";

        break;

        case "end":
       		$tx_step = "End";
      		$request= '';
      		$result = $_POST;
      		$message = "Transacion Finalizada";
      		$next_page='';
      		//aqui es posible hacer un redirect a la pagina de exito de webpay
      		if (!isset($result['token_ws'])) {
      			$file = fopen("../../../storage/webpay/".$result['TBK_ORDEN_COMPRA']."_webpay.log", "a+");
      			$array_tempo = $result;
      			array_push($array_tempo, "result de anulación ");
      			$output = print_r($array_tempo, true);
      			fwrite($file , $output);
      			fclose($file);

      			//aqui cuando se anula
      			// header('Location: /TheNotCompany/public/');
      			//recibo del result TBK_TOKEN, TBK_ID_SESION, TBK_ORDEN_COMPRA (ped_id)
      			header('Location: http://'.$_SERVER['HTTP_HOST'].'/WebpayAnulado/'.$result['TBK_ORDEN_COMPRA']);

      		}else{
      			//recibo de result  token_ws
      			//aqui esta la wea finalkizada 100% correcto. solo devuelve el token
      			header('Location: http://'.$_SERVER['HTTP_HOST'].'/WebpayExito/'.$result['token_ws']);
      		}
      		break;
}

echo "<h2>Step: " . $tx_step . "</h2>";

if (!isset($request) || !isset($result) || !isset($message) || !isset($next_page)) {

    $result = "Ocurri&oacute; un error al procesar tu solicitud";
    echo "<div style = 'background-color:lightgrey;'><h3>result</h3>$result;</div><br/><br/>";
    echo "<a href='.'>&laquo; volver a index</a>";
    die;
}

/* Respuesta de Salida - Vista WEB */
?>
<!DOCTYPE html>
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

		<style media="screen">
		body {
			font: 100% Verdana, Arial, Helvetica, sans-serif;
			background: #8B6BAF;
				background-repeat: repeat;
				margin: 0;
				padding: 0;
				text-align: center;
				color: #000000;
				font-size: 10pt;
 		}
		</style>
		<title></title>
	</head>
	<body>
		<h1 style="color:white;">Redireccionando</h1>
		<h2><?php //echo "Step: ".$tx_step ?></h2>
		<div style="background-color:lightyellow;">
<!-- 			<h3>request</h3> -->
			<?php //($request); ?>
		</div>
		<div style="background-color:lightgrey;">
<!-- 			<h3>result</h3> -->
			<?php  //var_dump($result); ?>
		</div>
		<p><samp><?php  //echo $message; ?></samp></p>
		<?php if (strlen($next_page)) { ?>
		<form action="<?php echo $next_page;  ?>" method="post" name="webpayForm">
			<input type="hidden" name="token_ws" value="<?php echo $token; ?>">
			<input type="submit" value="Continuar &raquo;">
		</form>
	</body>
</html>
<script language='JavaScript'>document.webpayForm.submit();</script>
<?php } ?>
<!-- <br> -->
<!-- <a href=".">&laquo; volver a index</a> -->
