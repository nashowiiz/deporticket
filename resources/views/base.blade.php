<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="csrf-token" id="token" content="{{ csrf_token() }}">
      <meta name="urlpagina" id="urlpagina" content="{{ url('/') }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{url('css/app.css?v2.7')}}">
      <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
      <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <title>Deporticket</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Montserrat|Raleway" rel="stylesheet">
    </head>

    <body>
        <div id="app" class="page">
          <!-- contenido -->
          @yield('contenido')
          <!-- fin contenido -->
        </div>
    </body>

    <script type="text/javascript" src="{{ url('js/app.js?v2.7') }}"></script>
    @yield('js-libraries')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131632630-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-131632630-1');
    </script>

    <?php include_once("facebook-live-chat/index.php");
    live_chat_facebook("https://www.facebook.com/Deporticket-2239033883001840", env('APP_URL'));
    ?>

</html>
