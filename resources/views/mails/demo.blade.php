<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    Hello <i>{{ $demo->sender }}</i>,
    <p>This is a demo email for testing purposes! Also, it's the HTML version.</p>

    <p><u>Demo object values:</u></p>

    <div>
    <p><b>Demo One:</b>&nbsp;adsdsa</p>
    <p><b>Demo Two:</b>&nbsp;sdadsasda</p>
    </div>

    <p><u>Values passed by With method:</u></p>

    <div>
    <p><b>testVarOne:</b>&nbsp;sadds</p>
    <p><b>testVarTwo:</b>&nbsp;dsasad</p>
    </div>

    Thank You,
    <br/>
    <i>asdsdsd</i>

  </body>
</html>
