@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<my-account :user="{{json_encode($user)}}"
            :purchases="{{json_encode($purchases)}}"
            :tickets_detail="{{json_encode($tickets_detail)}}"
            :purch_event="{{ json_encode($purch_event) }}"
            :update_user_url="{{ json_encode(route('update_site')) }}"
            :csrf_token="{{ json_encode(csrf_token()) }}">
</my-account>
<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
