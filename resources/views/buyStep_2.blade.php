@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<progress-bar :step="{{ json_encode(2) }}" ></progress-bar>
<buy-step-2 :event_id="{{ json_encode($event_id) }}"
            :forms="{{ json_encode($forms) }}"
            :user="{{ json_encode($user) }}"
            :csrf_token="{{ json_encode(Session::token()) }}"
            :css_class="{{ json_encode($class) }}"
            >
</buy-step-2>
<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
