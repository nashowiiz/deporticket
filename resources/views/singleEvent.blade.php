@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<single-event :user="{{json_encode($user)}}"
              :event="{{json_encode($event)}}"
              :login_url="{{ json_encode(route('login_site')) }}"
              :tickets="{{ json_encode($ticketsType) }}">
</single-event>
<!-- maps container -->
<gmaps-tag :lat="{{ json_encode($event['lat']) }}"
         :lng="{{ json_encode($event['lng']) }}"
         :address="{{ json_encode($event['place']) }}" >
</gmaps-tag>
<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
