<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <title>Confirmación Ticket, Deporticket</title>

  <!-- Normalize Styles -->
  <style type="text/css">
  /* What it does: Remove spaces around the email design added by some email clients. */
  /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
  html,
  body {
    margin: 0 auto !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
  }

  /* What it does: Stops WebKit and Windows mobile clients resizing small text. */
  * {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }

  .table1 {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
  }

  .table1 caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
  }

  .table1 tr {
    background-color: #f8f8f8;
    border: 1px solid #ddd;
    padding: .35em;
  }

  .table1 th,
  .table1 td {
    padding: .625em;
    text-align: center;
  }

  .table1 th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
  }

  @media screen and (max-width: 600px) {
    .table1 {
      border: 0;
    }

    .table1 caption {
      font-size: 1.3em;
    }

    .table1 thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    .table1 tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: .625em;
    }

    .table1 td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: .8em;
      text-align: right;
    }

    .table1 td::before {
      /*
      * aria-label has no advantage, it won't be read inside a .table1
      content: attr(aria-label);
      */
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }

    .table1 td:last-child {
      border-bottom: 0;
    }
  }

  /* What is does: Centers email on Android 4.4 */
  div[style*="margin: 16px 0"] {
    margin: 0 !important;
  }

  /* What it does: Remove spacing between tables in Outlook 2007 and up. */
  table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }

  /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. */
  table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
  }

  /* What it does: Reset styles. */
  img {
    line-height: 100%;
    outline: none;
    text-decoration: none;
    /* Uses a smoother rendering method when resizing images in IE. */
    -ms-interpolation-mode: bicubic;
    /* Remove border when inside `a` element in IE 8/9/10. */
    border: 0;
    /* Sets a maximum width relative to the parent and auto scales the height */
    max-width: 100%;
    height: auto;
    /* Remove the gap between images and the bottom of their containers */
    vertical-align: middle;
  }

  /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
  .yshortcuts a {
    border-bottom: none !important;
  }

  /* What it does: A work-around for iOS meddling in triggered links. */
  a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
  }

  /* What it does: Neutralize whitespace for inline-block grids on iOS. */
  @media screen and (min-width: 600px) {
    .ios-responsive-grid {
      display: -webkit-box !important;
      display: flex !important;
    }
    /* Alternative method. Not needed if already using the .ios-responsive-grid flex workaround. */
    /* .ios-responsive-grid__unit class would need to be added to the inline-block <div> grid units  */
    .ios-responsive-grid__unit  {
      float: left;
    }
  }

</style>


<!-- Progressive Enhancements -->
<style type="text/css">

/* Components */

/* What it does: Hover styles for buttons */
.button__td,
.button__a {
  transition: all 100ms ease;
}

.button__td:hover,
.button__a:hover {
  background: #1ab77b !important;
}



/* What it does: Mobile optimized styles */
@media screen and (max-width: 599px) {

  /* Components */

  .tw-card { padding: 20px !important; }
  .tw-h1 { font-size: 22px !important; }



  /* Utilities */

  /* Display */
  .mobile-hidden {
    display: none !important;
  }

  .mobile-d-block {
    display: block !important;
  }

  /* Size */
  .mobile-w-full {
    width: 100% !important;
  }

  /* Margin */
  .mobile-m-h-auto {
    margin: 0 auto !important;
  }

  /* Padding */
  .mobile-p-0 {
    padding: 0 !important;
  }

  .mobile-p-t-0 {
    padding-top: 0 !important;
  }

  /* Images */
  .mobile-img-fluid {
    max-width: 100% !important;
    width: 100% !important;
    height: auto !important;
  }
}

</style>
</head>


<body style="background: #ffffff; height: 100% !important; margin: 0 auto !important; padding: 0 !important; width: 100% !important; ;">
  <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all;">
  </div>

  <table cellpadding="0" cellspacing="0" style="background: #f2f2f2; border: 0; border-radius: 0; width: 100%;">
    <tbody><tr>
      <td align="center" class="" style="padding: 0 20px;">

        <table cellpadding="0" cellspacing="0" style="background: #f2f2f2; border: 0; border-radius: 0;">
          <tbody><tr>
            <td align="center" class="" style="width: 600px;">

              <table cellpadding="0" cellspacing="0" dir="ltr" style="border: 0; width: 100%;">
                <tbody><tr>
                  <td class="" style="padding: 20px 0; text-align: center;">
                  </td>
                </tr>

              </tbody></table>

              <table cellpadding="0" cellspacing="0" style="background: #ffffff; border: 0; border-radius: 4px; width: 100%;">
                <tbody><tr>
                  <td align="center" class="tw-card" style="padding: 20px 50px;">

                    <table cellpadding="0" cellspacing="0" dir="ltr" style="border: 0; width: 100%;">
                      <tbody><tr>
                        <td class="" style="padding: 20px 0; text-align: center;background: black;">

                          <img alt="" class=" " src="{{env('APP_URL')}}/images/logo-deporticket-black.jpg" style="border: 0; height: auto;width:250px; max-width: 100%; vertical-align: middle; ;" width="200">

                        </td>
                      </tr>

                    </tbody></table>

                    <table cellpadding="0" cellspacing="0" dir="ltr" style="border: 0; width: 100%;">

                      <tbody><tr>
                        <td class="" style="padding: 20px 0; text-align: left; color: #474747; font-family: sans-serif;;">



                          <p style="margin: 20px 0;; font-size: 14px; mso-line-height-rule: exactly; line-height: 24px; margin: 30px 0;; ;">



                            <span style="font-weight: bold;;">
                              El pedido fue realizado correctamente, su número de pedido es
                              <strong>Nº{{$data->idpedido}}</strong>.
                            </span>

                          </p>

                          <p style="margin: 20px 0;; font-size: 14px; mso-line-height-rule: exactly; line-height: 24px; margin: 30px 0;#332f2f !important;">
                            Hola! Gracias por confiar en nosotros, tu pedido ha sido procesado
                            correctamente. A continuación te mostraremos tu código QR con el cual deberas hacer ingreso al evento.

                            recuerda seguirnos en nuestras redes sociales para estar en contacto.
                          </p>


                          <p>A continuación detalle de su compra:</p>
                          <table class="table1">
                            <tbody>
                              <tr>
                                <td data-label="Account">{{$data->eventoname}}</td>
                                <td data-label="Due Date">{{$data->fechacompra}}</td>
                                <td data-label="Amount">${{$data->preciototal}}</td>
                                <td data-label="Period">cantidad de boletos {{$data->cantidad}}</td>
                              </tr>
                            </tbody>
                          </table>


                          <p style="margin: 20px 0;; font-size: 14px; mso-line-height-rule: exactly; line-height: 24px; margin: 30px 0;; margin: 45px 0 0; ;">
                            Tus entradas son generadas a través de un código único QR, y han sido enviadas
                            individualmente a los participantes de este evento. Revisa tu casilla de correo. <br>

                          </p>




                          <p style="margin: 20px 0;font-size: 14px; mso-line-height-rule: exactly; line-height: 24px; margin: 30px 0;; margin: 45px 0 0; ;">
                            Cualquier duda que tengas por favor ponte en contacto con nosotros. <br>
                            <span style="font-weight: bold;color:#070707 !important;"><a href="mailto:info@deporticket.cl">info@deporticket.cl</a></span>
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>


                </td>
              </tr>
            </tbody></table>



            <table cellpadding="0" cellspacing="0" dir="ltr" style="border: 0; width: 100%;">

              <tbody><tr>
                <td class="" style="padding: 20px 0; text-align: center; color: #8f8f8f; font-family: sans-serif; font-size: 12px; mso-line-height-rule: exactly; line-height: 20px;;">


                  <p style="margin: 20px 0;; margin: 0;;">

                    Sitio web creado por  <a href="https://turingdev.cl" style="color: #316fea; text-decoration: none;" target="_blank">turingdev</a> ♡ Con cariño!

                  </p>

                </td>
              </tr>

            </tbody></table>
          </td>
        </tr>
      </tbody></table>

    </td>
  </tr>
</tbody></table>

</body>
