@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<login-tag :register_url="{{ json_encode(route('register_site')) }}"
           :start_url="{{ json_encode(route('start_session')) }}"
           :csrf_token="{{ json_encode(csrf_token()) }}">
</login-tag>
<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
