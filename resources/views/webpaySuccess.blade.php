@extends('base')
@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<progress-bar :step="{{ json_encode(3) }}" ></progress-bar>

<buy-step-3 :status="1"
            :base_url="{{ json_encode(env('APP_URL')) }}"
            :invoice="{{ json_encode($invoice) }}"
            :tickets="{{ json_encode($tickets) }}"
            :event="{{ json_encode($event) }}"
            >
</buy-step-3>

<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
