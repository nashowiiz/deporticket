<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    .print-container {
      font-family: Helvetica, sans-serif;
max-width: 900px;
margin: 30px auto;
background: white;
padding: 10px 30px;
}
.print-container .header {
margin-bottom: 20px;

padding-bottom: 20px;
}
.print-container table {
margin-top: 30px;
}
.print-container table tbody tr.no-border:first-child {
opacity: 0.6 !important;
}
.print-container .summary-table {

}
.print-container .summary-table tr td:last-child {
text-align: right;
}
.print-container .summary-table tr th:last-child {
text-align: right;
}
.print-container .summary-table td {

}
.print-container .summary-table th {

}
.print-container .summary-table thead {
color: #737F8B;
}
.print-container .ft-18 {
font-size: 20px;
margin-bottom: 10px;
}
.print-container .adder {
font-size: 16px;
font-weight: 500;
text-align: right;
border-left: 0;
border-right: 0;
border-bottom: 0;
}
.print-container .total {
font-size: 22px;
}
.print-container .mega {
font-size: 33px;
margin-bottom: 10px;
}

.invoice-logo {
height: 80px;
width: auto;
}

.other-rates {
float: right;
width: 350px;
text-align: right;
}
.other-rates dl {
width: 100%;
margin-bottom: 5px;
}
.other-rates dl.total {
border-top: 1px solid #dbdbdb;
padding-top: 10px;
}
.other-rates dt {
width: 50%;
float: left;
}
.other-rates dd {
width: 50%;
float: left;
padding-right: 10px;
margin: 0;
}

.invoice-from {
float: right;
}

.summary-info {
margin-bottom: 20px;
padding-bottom: 10px;
}

.sub-heading .billto {
padding: 10px 0;
line-height: 20px;
}

@media print {
h1, h2, h3, h4, h5, h6 {
  font-weight: bold;
}
h1:first-letter, h2:first-letter, h3:first-letter, h4:first-letter, h5:first-letter, h6:first-letter {
  font-size: inherit;
}
}
.invoice-details {
float: right;
}

.ft-12 {
font-size: 12px;
}

</style>

  </head>
  <body>
    <div class="print-container clearfix">
  <div class="header">
    <div class="sub-header">
    <div class="content">
      <table style="width:100%">
        <tr style="width:100%" class="heading">
          <td colspan="2" style="background:black;text-align:center;padding:15px;border-radius: 20px;">
                      <img class="invoice-logo" src="{{ env('APP_URL') }}/images/logo-deporticket-black.jpg" alt="" />
          </td>
          <td>
            <div class="invoice-from" style="text-align:left;">

              <h3>Asistente:</h3>
              <h3>{{$dataemail->nombre}}</h3>
              <h3>Evento:</h3>
              <h3>{{$dataemail->nombreEvento}}</h3>

          </div>
        </td>
        </tr>
          <tr class="sub-heading">
            <td colspan="4">
                <div class="billto">
                  <big><strong>Deporticket SPA</strong></big> <br />
                  Nueva Las Condes 12.375 - Las Condes, Santiago. Chile.<br />
                  info@deporticket.cl <br />
                  T: (+562) 00000

                </div>
            </td>

        </tr>
      </table>
    </div>
  </div>
  <div class="body" style="display:block;margin-top:80px;">
    <div class="summary-info" style="margin-top:40px;">
         <h4>Acá tienes tú entrada QR para el ingreso al evento.</h4>
      <table class="table summary-table">
  <tbody>
    <tr class="simple">
      <th scope="row">
        <td colspan="3">
          <img src="{{$dataemail->codigoqrasistente}}" alt="" width="300">
        </td>
      </th>
    </tr>
  </tbody>
      </table>


    </div>
</div>
</div></div>

  </body>
</html>
