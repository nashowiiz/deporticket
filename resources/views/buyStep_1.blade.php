@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<progress-bar :step="{{ json_encode(1) }}" ></progress-bar>
<buy-step-1 :validate_code_url="{{ json_encode(route('validate_code_url')) }}"
            :event="{{ json_encode($event) }}"
            :tickets="{{ json_encode($available) }}"
            :user="{{ json_encode($user) }}"
            :csrf_token="{{ json_encode(Session::token()) }}">
</buy-step-1>
<featured-event :featured="{{json_encode($featuredEvents)}}"></featured-event>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
