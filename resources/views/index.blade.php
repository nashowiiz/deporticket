@extends('base')

@section('contenido')
<menu-tag :login_url="{{ json_encode(route('login_site')) }}"
          :account_url="{{json_encode(route('account_url')) }}"
          :search_url="{{ json_encode(route('search_url'))}}"
          :user="{{json_encode($user)}}"
          :csrf_token="{{ json_encode(csrf_token()) }}">
</menu-tag>
<header-tag :featured="{{ json_encode($featuredEvents) }}"
            :user="{{ json_encode($user) }}"
            :login_url="{{ json_encode(route('login_site')) }}">
</header-tag>
<featured-event :featured="{{json_encode($events)}}"></featured-event>
<content-tag :events="{{ json_encode($events) }}"
             :contact_url="{{ json_encode(route('contact')) }}"
             :csrf_token="{{ json_encode(csrf_token()) }}"></content-tag>
<footer-tag></footer-tag>
@endsection

@section('js-libraries')
@endsection
