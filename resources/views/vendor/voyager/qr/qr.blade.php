@extends('voyager::master')
<link rel="stylesheet" href="{{url('css/micss.css?v12.7')}}">
@section('content')
<div class="container-fluid" id="app">
<qr-component :url_burn_gd="{{ json_encode(route('burnGetData')) }}"
              :url_burn_ch="{{ json_encode(route('toggleBurnProp')) }}"
              :csrf_token="{{ json_encode(csrf_token()) }}"></qr-component>
</div>
@stop

@section('javascript')
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>
@stop
