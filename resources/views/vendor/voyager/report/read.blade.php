@extends('voyager::master')
@section('page_header')
    <h1 class="page-title">{{$event->title}}</h1>
@stop
@section('content')

<div class="container-fluid" id="app">
<content-report :event="{{ json_encode($event) }}"
                :route="{{ json_encode(route('getDataReport')) }}"
                :csrf_token="{{ json_encode(csrf_token()) }}">
</content-report>
</div>
@stop

@section('css')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
@stop

@section('javascript')
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>

<script>
$(document).ready(function() {
  setTimeout( function () {
    var table = $('#dataTable').DataTable({"order":[],
                "language":{"sEmptyTable":"No hay datos disponibles en la tabla",
                "sInfo":"Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "sInfoEmpty":"Mostrando 0 a 0 de 0 entradas",
                "sInfoFiltered":"(Filtrada de _MAX_ entradas totales)",
                "sInfoPostFix":"","sInfoThousands":",",
                "sLengthMenu":"Mostrar _MENU_ entradas","sLoadingRecords":"Cargando...",
                "sProcessing":"Procesando...","sSearch":"Buscar:",
                "sZeroRecords":"No se encontraron registros coincidentes",
                "oPaginate":{"sFirst":"Primero","sLast":"\u00daltimo",
                "sNext":"Siguiente","sPrevious":"Anterior"},
                "oAria":{"sSortAscending":": Activar para ordenar la columna ascendente",
                "sSortDescending":": Activar para ordenar la columna descendente"}},
                "columnDefs":[{"targets":-1,"searchable":false,"orderable":false}]});

    }, 500 );

});
</script>
@stop
