@extends('voyager::master')
@section('page_header')
    <h1 class="page-title">{{$current['title']}}</h1>
@stop
@section('content')
<div class="container-fluid" id="app">
<select-users :events='{{ json_encode($events) }}'
              :current="{{ $current }}"></select-users>
</div>
@stop

@section('javascript')
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>
@stop
