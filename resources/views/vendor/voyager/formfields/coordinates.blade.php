<style>
    #map {
        height: 400px;
        width: 100%;
    }
</style>
@forelse($dataTypeContent->getCoordinates() as $point)
    <input type="hidden" name="{{ $row->field }}[lat]" value="{{ $point['lat'] }}" id="lat"/>
    <input type="hidden" name="{{ $row->field }}[lng]" value="{{ $point['lng'] }}" id="lng"/>
@empty
    <input type="hidden" name="{{ $row->field }}[lat]" value="{{ config('voyager.googlemaps.center.lat') }}" id="lat"/>
    <input type="hidden" name="{{ $row->field }}[lng]" value="{{ config('voyager.googlemaps.center.lng') }}" id="lng"/>
@endforelse

<script type="application/javascript">
    //redefine global (myown)
    var map;
    var marker;
    var autocomplete;

    function initMap() {
        @forelse($dataTypeContent->getCoordinates() as $point)
            var center = {lat: {{ $point['lat'] }}, lng: {{ $point['lng'] }}};
        @empty
            var center = {lat: {{ config('voyager.googlemaps.center.lat') }}, lng: {{ config('voyager.googlemaps.center.lng') }}};
        @endforelse
            map = new google.maps.Map(document.getElementById('map'), {
            zoom: {{ config('voyager.googlemaps.zoom') }},
            center: center
        });
        var markers = [];
        @forelse($dataTypeContent->getCoordinates() as $point)
            marker = new google.maps.Marker({
                    position: {lat: {{ $point['lat'] }}, lng: {{ $point['lng'] }}},
                    map: map,
                    draggable: true
                });
            markers.push(marker);
        @empty
            marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    draggable: true
                });
        @endforelse

        function initialize() {
          var input = document.getElementById('autocomplete');
          autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
          autocomplete.addListener('place_changed', geolocation);
        }

        google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('lat').value = this.position.lat();
            document.getElementById('lng').value = this.position.lng();
        });
        google.maps.event.addDomListener(window, 'load', initialize);

    }


    function geolocation(){

      var place = autocomplete.getPlace();

      if(place.name){
        marker.setPosition(place.geometry.location);
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;
      }
    }



</script>
<div id="map"/>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('voyager.googlemaps.key') }}&callback=initMap&libraries=places"></script>
