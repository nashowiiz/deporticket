@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
          <!-- form start -->
          <form role="form"
                  class="form-edit-add"
                  action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                  method="POST" enctype="multipart/form-data">
              <!-- PUT Method if we are editing -->
              @if(!is_null($dataTypeContent->getKey()))
                  {{ method_field("PUT") }}
              @endif

              <!-- CSRF TOKEN -->
              {{ csrf_field() }}

            <div class="col-md-8">
              <div class="panel panel-info panel-bordered">
                <div class="panel-heading">
                  <h3 class="panel-title">Información del Evento</h3>
                </div>
                <div class="panel-body">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                  @endif

                  <!-- Adding / Editing -->
                  @php
                      $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                  @endphp

                  @foreach($dataTypeRows as $row)
                      <!-- GET THE DISPLAY OPTIONS -->
                      @php
                          $display_options = isset($row->details->display) ? $row->details->display : NULL;
                      @endphp
                      @if (isset($row->details->legend) && isset($row->details->legend->text))
                          <legend class="text-{{isset($row->details->legend->align) ? $row->details->legend->align : 'center'}}" style="background-color: {{isset($row->details->legend->bgcolor) ? $row->details->legend->bgcolor : '#f0f0f0'}};padding: 5px;">{{$row->details->legend->text}}</legend>
                      @endif
                      @if (isset($row->details->formfields_custom))
                          @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                      @else
                          <?php  //Filter field print
                                 if($row->id == 139 ||
                                    $row->id == 185 ||
                                    $row->id == 128 ||
                                    $row->id == 114 ||
                                    $row->id == 151 ||
                                    $row->id == 127 ||
                                    $row->id == 138 ||
                                    $row->id == 258 ||
                                    $row->id == 236 ||
                                    $row->id == 266 ||
                                    $row->id == 234 ||
                                    //img
                                    $row->id == 270 ||
                                    $row->id == 271 ||
                                    $row->id == 272 ||
                                    $row->id == 259 ||
                                    $row->id == 258 ||
                                    $row->id == 315){
                                  continue; } ?>
                          <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                              {{ $row->slugify }}
                              <label for="name">{{ $row->display_name }} <?php echo $row->id;?></label>
                              @if($row->id == 122)
                                <input id="autocomplete"  autocomplete="off" type="text"  class="form-control" name="address" placeholder="Dirección" value="{{$dataTypeContent->address}}">
                              @else
                                @include('voyager::multilingual.input-hidden-bread-edit-add')
                                @if($row->type == 'relationship')
                                    @include('voyager::formfields.relationship', ['options' => $row->details])
                                @else
                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                @endif

                                @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                    {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                @endforeach
                              @endif
                          </div>
                      @endif
                  @endforeach

              </div><!-- panel-body -->
            </div> <!-- end panel bordered-->
          </div> <!-- end left column -->
        </div>

          <div class="col-md-4">
            <div class="panel panel-warning panel-bordered">
              <div class="panel-heading">
                <h3 class="panel-title">Atributos</h3>
              </div>
              <div class="panel-body">
                @foreach($dataTypeRows as $row)
                    <!-- GET THE DISPLAY OPTIONS -->
                    @php
                        $display_options = isset($row->details->display) ? $row->details->display : NULL;
                    @endphp
                    @if (isset($row->details->legend) && isset($row->details->legend->text))
                        <legend class="text-{{isset($row->details->legend->align) ? $row->details->legend->align : 'center'}}" style="background-color: {{isset($row->details->legend->bgcolor) ? $row->details->legend->bgcolor : '#f0f0f0'}};padding: 5px;">{{$row->details->legend->text}}</legend>
                    @endif
                    @if (isset($row->details->formfields_custom))
                        @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                    @else
                        <?php  //Filter field print
                               if($row->id == 112 ||
                                  $row->id == 111 ||
                                  $row->id == 113 ||
                                  $row->id == 117 ||
                                  $row->id == 209 ||
                                  $row->id == 118 ||
                                  $row->id == 122 ||
                                  $row->id == 125 ||
                                  $row->id == 210 ||
                                  $row->id == 258 ||
                                  $row->id == 267 ||
                                  //img
                                  $row->id == 270 ||
                                  $row->id == 271 ||
                                  $row->id == 272 ||
                                  $row->id == 259 ||
                                  $row->id==258){

                                continue; } ?>
                        <div class="form-group @if($row->type == 'hidden') hidden @endif  col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                            {{ $row->slugify }}



                            <label for="name">{{ $row->display_name }}</label>

                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                            @if($row->type == 'relationship')
                                @include('voyager::formfields.relationship', ['options' => $row->details])
                            @else
                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                            @endif

                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                            @endforeach
                        </div>
                    @endif
                @endforeach
              </div>
            </div>
            <div class="panel panel-warning panel-bordered">
              <div class="panel-heading">
                <h3 class="panel-title">Imagenes</h3>
              </div>
              <div class="panel-body">
                @foreach($dataTypeRows as $row)

                    <!-- GET THE DISPLAY OPTIONS -->
                    @php
                        $display_options = isset($row->details->display) ? $row->details->display : NULL;
                    @endphp
                    @if (isset($row->details->legend) && isset($row->details->legend->text))
                        <legend class="text-{{isset($row->details->legend->align) ? $row->details->legend->align : 'center'}}" style="background-color: {{isset($row->details->legend->bgcolor) ? $row->details->legend->bgcolor : '#f0f0f0'}};padding: 5px;">{{$row->details->legend->text}}</legend>
                    @endif
                    @if (isset($row->details->formfields_custom))
                        @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                    @else
                        <?php  //Filter field print
                          if($row->id != 270 and
                             $row->id != 271 and
                             $row->id != 272 and
                             $row->id != 259 and
                             $row->id != 258){
                          continue; } ?>
                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                            {{ $row->slugify }}

                            <label for="name">{{ $row->display_name }}</label>
                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                            @if($row->type == 'relationship')
                                @include('voyager::formfields.relationship', ['options' => $row->details])
                            @else
                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                            @endif

                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                            @endforeach
                        </div>
                    @endif
                @endforeach
              </div>
            </div>
          </div> <!-- end righ columun -->

          <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
          </div>

          </form>

          <iframe id="form_target" name="form_target" style="display:none"></iframe>
          <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                  enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
              <input name="image" id="upload_file" type="file"
                       onchange="$('#my_form').submit();this.value='';">
              <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
              {{ csrf_field() }}
          </form>
        </div><!-- end row form -->

      </div>


    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')

    <script>
        var params = {};
        var $image;

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();


            /*
            $("#autocomplete").focusin(function(){
              geolocation();
            });
            $("#autocomplete").blur(function(){
              geolocation();
            });*/


        });//End ready




    </script>
@stop
