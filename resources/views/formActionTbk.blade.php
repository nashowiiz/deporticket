@extends('base')
@section('contenido')
<form action="{{$returnResponse['formAction']}}" method="post" id="tbk-form">
  <input type="hidden" name="token_ws" value="{{$returnResponse['tokenWs']}}">
</form>
@endsection
@section('js-libraries')
<script>
  function load(){
    document.getElementById("tbk-form").submit();
  }
  window.onload = load();
</script>
@endsection
