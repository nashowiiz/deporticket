/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./Axios');
window.Vue = require('vue')
window.Vuex = require('vuex')

//import jQuery and instance noConflict for Voyager jquery version
window.$ = window.jQuery = require('jquery')
$.noConflict()

import * as VueGoogleMaps from "vue2-google-maps"

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCIfj0iw0NlIkUaIoHukoaC6xNEG1Z-Fzg",
    libraries: "places" // necessary for places input
  }
})

//import plug Axios
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

//Import Moment / es
const moment = require('moment')
require('moment/locale/es')
Vue.use(require('vue-moment'), {moment})
//carousel owlCarousel
import carousel from 'v-owl-carousel'
Vue.use(carousel)
//lector de tickets qr
import VueQrcodeReader from 'vue-qrcode-reader'
Vue.use(VueQrcodeReader)
//Toasted
import Toasted from 'vue-toasted'
Vue.use(Toasted, {
    iconPack : 'fontawesome' // set your iconPack, defaults to material. material|fontawesome
})
Vue.use(require('vue-moment'))
//Admin Voyager Requires
import VueCountdownTimer from 'vuejs-countdown-timer'
Vue.use(VueCountdownTimer)

//Vee Validate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

//TicketSport Theme
Vue.component('menu-tag', require('./components/ticketsport/Menu.vue'))
Vue.component('header-tag', require('./components/ticketsport/Header.vue'))
Vue.component('content-tag', require('./components/ticketsport/Content.vue'))
Vue.component('footer-tag', require('./components/ticketsport/Footer.vue'))
//Maps
Vue.component('gmaps-tag', require('./components/ticketsport/GoogleMap.vue'));
//View Single Event
Vue.component('single-event', require('./components/ticketsport/single-event/Header.vue'))
Vue.component('featured-event', require('./components/ticketsport/single-event/FeaturedEvents.vue'))
//Buy Steps
Vue.component('buy-step-1', require('./components/ticketsport/buy/Step_1.vue'))
Vue.component('buy-step-2', require('./components/ticketsport/buy/Step_2.vue'))
Vue.component('buy-step-3', require('./components/ticketsport/buy/Step_3.vue'))
Vue.component('progress-bar', require('./components/ticketsport/buy/ProgressBar.vue'))
//Auth
Vue.component('login-tag', require('./components/ticketsport/auth/Login.vue'))
//MyAccount
Vue.component('my-account', require('./components/ticketsport/MyAccount.vue'))

//ADMIN Voyager
Vue.component('content-report', require('./components/admin/ContentReport.vue'))
Vue.component('count-down', require('./components/admin/CountDown.vue'))
Vue.component('qr-component', require('./components/admin/Qr.vue'))
Vue.component('select-users', require('./components/admin/SelectUsers.vue'))
//Chars
Vue.component('e-chart', require('./components/admin/charts/EChart.vue'))
Vue.component('e-chartBar', require('./components/admin/charts/EChartBar.vue'))



const app = new Vue({
    el: '#app',
});
