<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assistant extends Model
{
    protected $table = 'assistants';
    protected   $fillable  = ['name','rut', 'email','phone','gender'];
    protected $casts = ['extra_fields' => 'array'];

    public function ticket(){
        return $this->hasMany('App\Ticket');
    }
}
