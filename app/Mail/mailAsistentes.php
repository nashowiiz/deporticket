<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailAsistentes extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */

  public $data;


  public function __construct($data)
  {
      $this->data = $data;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
   public function build()
   {
       return $this->from(env('MAIL_FROM_ADDRESS'))
                   ->view('email.qrasistentes')
                   ->subject($this->data->subject)
                     ->attach(public_path().'/storage/pdf/'.$this->data->nombrePDF , [
                             'as' => $this->data->nombrePDF,
                             'mime' => 'application/pdf',
                     ]);
   }
}
