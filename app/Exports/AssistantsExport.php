<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;

class AssistantsExport implements FromQuery,
                                  WithHeadings,
                                  ShouldAutoSize,
                                  WithMapping
{
    use Exportable;

    public function __construct(int $id)
    {
      $this->id = $id;
    }

    public function query()
    {
      $where = [['tickets.event_id',$this->id],['tickets.valid',1]];

      return DB::table('tickets')
         ->join('assistants','tickets.assistants_id','=','assistants.id')
         ->join('categories_tk','tickets.category_tk_id','=','categories_tk.id')
         ->select('assistants.name','rut','email','phone','status','extra_fields',
                  'assistants.gender','categories_tk.name as cat_name')
         ->where($where)
         ->orderBy('gender','desc');

    }

    public function headings(): array
   {
       return [
           'Nombre',
           'Rut',
           'Email',
           'Telefono',
           'Estado',
           'Género',
           'Categoria',
           'Extra'
       ];
    }

    public function map($assistants): array
    {
        $result = [
          $assistants->name,
          $assistants->rut,
          $assistants->email,
          $assistants->phone,
          $assistants->status,
          $assistants->gender,
          $assistants->cat_name,
        ];

        $extra_fields = json_decode($assistants->extra_fields, true);
        foreach ($extra_fields as $row) {
          $result[]=$row['key'].': '.$row['value'];
        }

        return $result;
    }


}//End class
