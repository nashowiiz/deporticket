<?php

namespace App\Imports;

use App\Result;
use Maatwebsite\Excel\Concerns\ToModel;

class ResultImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      return new Result([
                  'events_id' => $row[0],
                  'rider_name'=>$row[1],
                  'distance' => $row[2],
                  'category'=> $row[3],
                  'tmp_official' => $row[4],
                  'tmp_chip' => $row[5],
                  'lg_category' => $row[6],
                  'lg_general' => $row[7],
                  'lg_gender' => $row[8]
                ]);
          }
}
