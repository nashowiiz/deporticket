<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTickets extends Model
{
    protected $table = 'invoices_tickets';

    public function invoice(){
      return $this->belongsTo('App\Invoice', 'invoice_id');
    }

    public function ticket(){
      return $this->belongsTo('App\Ticket', 'ticket_id');
    }
}
