<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\User;
use App\Mail\mailRegistro;
use Mail;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }

    //Site functions Auth
    public function logout()
    {
      Auth::logout();
      return redirect($this->redirectTo);
    }

    //POST, Json response (false or User obj)
    public function axiosLogin(Request $request){
      $credentials = $request->only('email', 'password');
      if (Auth::attempt($credentials)) {
        $user = Auth::user();
        return response()->json(json_encode($user));
      } else {
        return response()->json(0);
      }
    }

    //POST, Json reponse (false or User obj)
    public function axiosRegister(Request $request){
      //Validate email exist
      $validate = User::where('email', $request->email)->get();
      if(count($validate)>=1){
        return response()->json("Este email ya se encuentra registrado");
        return false;
      }
      $newUser = new User;
      $newUser->name = $request->name;
      $newUser->email = $request->email;
      $newUser->password= Hash::make($request->password);

      if($newUser->save()){
        $data = new \stdClass();
        $data->name = $newUser->name;
        $data->email = $newUser->email;
        $data->subject = "Bienvenido a deporticket";

        Mail::to($data->email)->send(new mailRegistro($data));
        Auth::login($newUser);
        return response()->json(json_encode(1));
      }

      return false;


    }



    //Facebook functions
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /*
     _ Obtain the user information from provider.  Check if the user already exists in our
     _ database by looking up their provider_id in the database.
     _ If the user exists, log them in. Otherwise, create a new user then log them in. After that
     _ redirect them to the authenticated users homepage.
     _
     _ @return Response
     _/
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    /*
     _ If a user has registered before using social auth, return the user
     _ else, create a new user object.
     _ @param  $user Socialite user object
     _ @param $provider Social auth provider
     _ @return  User
   */

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
}
