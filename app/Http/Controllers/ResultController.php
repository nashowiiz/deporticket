<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;
use DB;
use App\Invoice;
use App\Ticket;
use App\TicketsType;
use App\Event;
use App\Assistant;
use Auth;
use Mail;
use App\Mail\mailRegistro;
use App\Mail\mailAsistentes;
use App\Mail\webpayExito;
use PDF;

class ResultController extends Controller
{

  //fields event
  private $fields =  'id,title,description,content,img_vertical,img_sponsor,
                      img_vertical_panel,img_horizontal_panel,img_square_panel,
                      start_time,end_time,owner,slug,place,service_value,
                      start_date,end_date,address,categories_id,
                      X(coordinates) AS "lat",
                      Y(coordinates) AS "lng"';

  public function webpayResult(Request $request){
    $user = Auth::user();

    //instance transaction
    $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))
                                              ->getNormalTransaction();
    $result = $transaction->getTransactionResult($request->input("token_ws"));
    $output = $result->detailOutput;

    if ($output->responseCode == 0) {
      //Success, save order on session var nº
      $request->session()->put('order', $output->buyOrder);
      $argument = $this->successProcess($output, $user);
      $returnResponse= array('formAction' => $result->urlRedirection,
                             'tokenWs' => $request->input('token_ws'));
      return view('formActionTbk', compact('returnResponse'));

    }else{
          //Fail TBK
          //get last events
          $query = 'SELECT '.$this->fields.'
                   FROM events
                   WHERE status = 1
                   LIMIT 7';
          $featuredEvents = DB::select($query);
          return view('webpayFail', compact('featuredEvents','user'));
        }
  }

  function finalProcessTbk(Request $request){
    $user = Auth::user();

    if($request->session()->get('order')){
      //Success Continues
      //Get invoice
      $invoice= Invoice::findOrFail($request->session()->get('order'));
      //Get Invoice with rel rel
      $invoice_it = Invoice::with('invoiceTicket')
                           ->findOrFail($request->session()->get('order'));
      //Clear var
      $request->session()->forget('order');
      //Get Tickets
      foreach ($invoice_it->invoiceTicket as $it) {
        $tickets[] = Ticket::with('ticketType')->findOrFail($it->ticket_id);
      }
      //Get event of this Invoice
      $event = Event::select(DB::raw($this->fields))
                          ->where('id',$tickets[0]->event_id)
                          ->first();
      //get last events
      $query = 'SELECT '.$this->fields.'
               FROM events
               WHERE status = 1
               LIMIT 7';
      $featuredEvents = DB::select($query);

      //Success
      return view('webpaySuccess', compact('user',
                                           'invoice',
                                           'tickets',
                                           'event',
                                           'featuredEvents'));
    } else {
      //Cancel Transaction
      //get last events
      $query = 'SELECT '.$this->fields.'
               FROM events
               WHERE status = 1
               LIMIT 7';
      $featuredEvents = DB::select($query);
      return view('webpayFail', compact('featuredEvents','user'));
    }


  }



  //Helpers functions
  function successProcess($response, $user){

    //Get invoice  + ticket + update satatus true (pagado)
    $invoice = Invoice::with('invoiceTicket')
                      ->findOrFail($response->buyOrder);
    $invoice->status = 1;
    $invoice->save();

    //Get all tickets of this invoice
    foreach ($invoice->invoiceTicket as $it) {
      //Get this ticket
      $ticket = Ticket::with('ticketType')->findOrFail($it->ticket_id);
      $ticket->valid=1;
      $ticket->save();
      $tickets[] = $ticket;

      //Update Available Ticket (decrement ticket type)
      $ticket->ticketType->available_tickets=
      $ticket->ticketType->available_tickets-1;
      $ticket->ticketType->save();

      //Get Assistant of this ticket
      $assistant = Assistant::findOrFail($ticket->assistants_id);
      //Get Event of this ticket
      $event = Event::findOrFail($ticket->event_id);

      //Send e-mails
      $dataemail = new \stdClass();
      $dataemail->subject = "Tú entrada al evento ".$event->title;
      $dataemail->nombreEvento = $event->title;
      $dataemail->nombre = $assistant->name;
      $dataemail->email = $assistant->email;
      $dataemail->qrcodes = $ticket->qrcode;
      $dataemail->codigoqrasistente=public_path().$ticket->qrcode;
      $dataemail->nombrePDF = $assistant->name."-".$assistant->id.".pdf";

      $pdf = PDF::loadView('qrpdf', compact('dataemail'))
                   ->save( 'storage/pdf/'.$dataemail->nombrePDF );
      //store pdf route
      $ticket->pdf=$dataemail->nombrePDF;
      $ticket->save();

      Mail::to($dataemail->email)
          ->send(new mailAsistentes($dataemail));
    } //End for


    //Send customer email detail
    $data = new \stdClass();
    $data->username = $user->name;
    $data->email = $user->email;
    $data->subject = "Compra realizada correctamente!";
    $data->codigosqr = $tickets;
    $data->idpedido = $response->buyOrder;
    $data->fechacompra = $invoice->created_at;
    $data->eventoname = $event->name;
    $data->preciototal = $invoice->total_price;
    $data->cantidad = count($invoice->invoiceTicket);

    Mail::to($data->email)->send(new webpayExito($data));

    return array('invoice'=>$invoice,
                 'event' => $event);


  }//End function

}
