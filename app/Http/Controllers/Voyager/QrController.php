<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Checklist;
use App\Ticket;
use App\Assistant;
use App\Prop;
use App\Event;

class QrController extends VoyagerBaseController
{
  public function index(Request $request)
  {
    return view('vendor/voyager/qr/qr');

  }

  public function burnGetData(Request $request){

    //Get ticket and burn
    $ticket = Ticket::findOrFail($request->id);
    $ticket->status = 1; //Burn
    $ticket->save();

    //Get event title
    $event = Event::findOrFail($ticket->event_id);

    //Get assistant
    $assistant = Assistant::findOrFail($ticket->assistants_id);
    //Get props
    $props = Checklist::where('ticket_id',$ticket->id)->get();
    foreach ($props as $p) {
      $prop = Prop::findOrFail($p->prop_id);
      $props_list[] = array('id_tk' => $ticket->id,
                            'id_ch' => $p->id,
                            'name'=>$prop->name,
                            'status'=>$p->status);
    }
    $allData = array('props_list'=> $props_list,
                     'assistant'=>$assistant,
                     'title' => $event->title);

    return response()->json($allData);
  }

  public function toggleBurnProp(Request $request){

    //toggleBurnProp
    $check = Checklist::findOrFail($request->id_ch);
    if($check->status==0){
      $check->status=1;
    } else {
      $check->status=0;
    }
    $check->save();

    //Get props
    $props = Checklist::where('ticket_id',$request->id_tk)->get();

    foreach ($props as $p) {
      $prop = Prop::findOrFail($p->prop_id);
      $props_list[] = array('id_tk' => $request->id_tk,
                            'id_ch' => $p->id,
                            'name'=>$prop->name,
                            'status'=>$p->status);
    }

    return response()->json($props_list);

  }

}// End class
