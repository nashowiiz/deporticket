<?php

namespace App\Http\Controllers\Voyager;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

//Import Excel export libraries
use App\Exports\AssistantsExport;
use Maatwebsite\Excel\Facades\Excel;

//Import Models
use App\Event;
use App\Assistant;
use App\Ticket;
use App\TicketsType;
use App\Invoice;

class CheckinController extends VoyagerBaseController
{

    use BreadRelationshipParser;

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        //$slug = $this->getSlug($request);

        //Own slug
        $slug = "events";

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by');
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $relationships = $this->getRelationships($dataType);

            $model = app($dataType->model_name);
            $query = $model::select('*')->with($relationships);

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }
         //dd($view);
        //Change to my own view
        $view = "voyager::report.browse";
        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'sortOrder',
            'searchable',
            'isServerSide'
        ));
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************

    public function show(Request $request, $id)
    {
        $slug = 'report';
        $view = "voyager::$slug.read";
        $fields = 'id,start_date,start_time,end_date,end_time,title';
        $event=Event::select(DB::raw($fields))
                                ->where('id',$id)
                                ->first();

        return Voyager::view($view, compact('event'));
    }

    /* Data report*/
    function getDataReport(Request $request)
    {
      //Get Models and kpi's
      //Get event
      $event = Event::find($request->id);
      //return response()->json(json_encode($request->id));

      //Get ticket type
      $ticketsType = TicketsType::where('event_id',$event->id)
                                ->get();

      $where = [['event_id',$event->id],['valid',1]];
      $tickets = Ticket::with('assistant', 'user')
                       ->where($where)
                       ->get();

      $where = [['tickets.event_id',$event->id],['tickets.valid',1]];
      $assistants = DB::table('tickets')
               ->join('assistants','tickets.assistants_id','=','assistants.id')
               ->leftjoin('categories_tk','tickets.category_tk_id','=','categories_tk.id')
               ->select('assistants.name','rut','email','phone','status',
                        'assistants.gender','categories_tk.name as cat_name')
               ->where($where)
               ->orderBy('gender','desc')
               ->get();

      //Get Inovices event
      $invoices = Invoice::WHERE("event_id",$event->id)->get();

      //Total Discount
      $total_discount=null;
      $total_service=null;
      $total_sold = null;

      foreach ($invoices as $invoice) {
        if($invoice->discount){
          $total_absolute = (100000000 * $invoice->total_price)/
                            (100000000 - 1000000 * $invoice->discount -
                             10000 * $event->service_value * $invoice->discount+
                             1000000 * $event->service_value);

          $service = ($total_absolute/100)*$event->service_value;
          $discount = (($service+$total_absolute)/100)*
                            $invoice->discount;
          $total_service = $total_service + $service;
          $total_discount = $total_discount + $discount;
        } else {
          //Only service
          $total_service = $total_service +
                           $invoice->total_price/(100+$event->service_value)*
                           $event->service_value;
        }

        $total_sold = $total_sold + $invoice->total_price;
      }

      $totals = array('total_service' => $total_service,
                      'total_discount' => $total_discount,
                      'total_sold' => $total_sold);

      $allData = array('ticketsType' => $ticketsType,
                       'tickets'=>$tickets,
                       'assistants' => $assistants,
                       'totals' => $totals);

      return response()->json($allData);
    }


    //My own funciton helper
    protected function getDataByModel($slug,$id){
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model->with($relationships), 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return array("dataType" => $dataType,
                     "dataTypeContent" => $dataTypeContent,
                     "isModelTranslatable" => $isModelTranslatable);
    }

    //Export Excel Asistant liste
    public function ExportAssistantList(Request $request, $id){
      return Excel::download(new AssistantsExport($id),'Asistentes_'.$id.'.xlsx');
    }

}//End class controller
