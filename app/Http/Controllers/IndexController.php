<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Event;
use App\Category;
use App\User;
use App\Assistant;
use App\Invoice;
use App\InvoiceTickets;
use App\Ticket;
use App\TicketsType;
use App\EventRules;
use App\Checklist;
use App\Prop;
use App\Discount;
use Hash;
use Auth;
use Mail;
use App\Mail\mailContact;
use Redirect;
use SoapClient;
use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;
use QrCode;
use PDF;

class IndexController extends Controller
{

  //fields event
  private $fields =  'id,title,description,content,img_vertical,img_sponsor,
                      img_vertical_panel,img_horizontal_panel,img_square_panel,
                      start_time,end_time,owner,slug,place,service_value,
                      start_date,end_date,address,categories_id,
                      X(coordinates) AS "lat",
                      Y(coordinates) AS "lng"';

    public function index(){
      $user = Auth::user();
      //get last 100 events
      $query = 'SELECT '.$this->fields.'
                FROM events
                LIMIT 100';
      $events = DB::select($query);
      //get featured events
      $query = 'SELECT '.$this->fields.'
                FROM events
                WHERE featured = 1
                AND status = 1
                LIMIT 5';
      $featuredEvents = DB::select($query);
      //Return view and send argument
      return view('index', compact('user',
                                  'events',
                                  'featuredEvents'));
    }

    //Display login
    public function displayLogin(){
      $user = Auth::user();
      //get last 7
      $query = 'SELECT '.$this->fields.'
                FROM events
                LIMIT 7';
      $featuredEvents = DB::select($query);
      //Return view and send argument
      return view('/loginForm', compact('user','featuredEvents'));

    }

    //Display module myaccount
    public function myAccount(Request $request){
      //Set props for vue
      $tickets_detail=null;
      $purch_event=null;
      $user = Auth::user();
      if(!$user) return redirect('/site/login');
      //Get Invoices
      $where = [['user_id',$user->id],['status',1]];
      $purchases = Invoice::with('invoiceTicket')
                          ->where($where)
                          ->get();
      //Get Detail tickets
      foreach ($purchases as $purch) {

        foreach ($purch->tickets_quantity as $key => $value) {
          //make out array (ticket_type + quantity)
          if($value==0) continue;
          $tp = TicketsType::findOrFail($key);
          $event_id = $tp->event_id;
          $tickets_detail[$purch->id][$key]['name']= $tp->name;
          $tickets_detail[$purch->id][$key]['price']= $tp->price;
          $tickets_detail[$purch->id][$key]['quantity']=$value;
          //Get tickets
          foreach ($purch->invoiceTicket as $rel) {
            $tickets_detail[$purch->id][$key]['tickets'][]=
            Ticket::findOrFail($rel->ticket_id);
          }

        }
        //Get event purch
        $purch_event[$purch->id]=Event::select(DB::raw($this->fields))
                                        ->where('id', $event_id)
                                        ->first();
      }
      //get last events
      $query = 'SELECT '.$this->fields.'
               FROM events
               WHERE status = 1
               LIMIT 7';
      $featuredEvents = DB::select($query);


      return view('myAccount', compact('user',
                                       'purchases',
                                       'tickets_detail',
                                       'purch_event',
                                       'featuredEvents'));
    }

    //Single Event
    public function event($slug){
      $user = Auth::user();

      //Get event + rules
      $event = Event::select(DB::raw($this->fields))
                          ->with("eventRules")
                          ->where('slug', $slug)
                          ->first();

      //Get tickets type + props
      $ticketsType =  TicketsType::with('props')
                                 ->where('event_id',$event->id)
                                 ->get();
      //get last events
      $query = 'SELECT '.$this->fields.'
                FROM events
                WHERE status = 1
                LIMIT 7';
      $featuredEvents = DB::select($query);

      return view('singleEvent', compact('user',
                                         'event',
                                         'ticketsType',
                                         'featuredEvents'));
    }

    //Show tickets product
    public function buyTickets($slug){
      $user = Auth::user();
      if(!$user) return redirect('/site/login');
      //Get event + rules
      $event = Event::select(DB::raw($this->fields))
                          ->with("eventRules")
                          ->where('slug', $slug)
                          ->first();
      //Get tickets type + props
      $ticketsType =  TicketsType::with('props')
                                 ->where('event_id',$event->id)
                                 ->get();
      //Check available ticket
      $available = [];
      foreach ($ticketsType as $ticket) {
        if($ticket->available_tickets > 0){
          $available[] = $ticket;
        }
      }
      //Deactivate event
      if(!count($available)){
        $event->status=0;
        $event->save();
      }
      //get last events
      $query = 'SELECT '.$this->fields.'
               FROM events
               WHERE status = 1
               LIMIT 7';
      $featuredEvents = DB::select($query);

      return view('buyStep_1', compact('event',
                                      'available',
                                      'featuredEvents',
                                      'user'));
    }

    //Make Forms
    public function buyForms(Request $request){
      $user = Auth::user();
      if(!$user) return redirect('/site/login');

      //Post quantity tickets
      $quantities = $request->quantities;
      //store detail quantity and discount on session var
      $request->session()->put('quantities', $request->quantities);
      $request->session()->put('discount', $request->discount);
      $request->session()->put('total_price', $request->total_price);
      //Post Total price
      $total_price = $request->total_price;
      //Generate Forms ticket
      foreach ($quantities as $key => $value) {
        for ($i=1; $i<=$value; $i++) {
          //Get TicketsType + extraFields
          $ticket_ = TicketsType::with('extraFields')->findOrFail($key);
          $fields = null;
          foreach ($ticket_->extraFields as $extra) {
            $fields[] = $extra->key;
          }
          $forms[]=array('id' => $ticket_['id'],
                         'name' => $ticket_['name'],
                         'extra' => $fields);
        }
      }
      //Div class columns
      if(count($forms)>2){
        $class= array('container' => "col-md-12 col-lg-12",
                      'columns' => "col-md-4 col-lg-4");
      }
      if(count($forms)==2){
        $class= array('container' => "col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2",
                      'columns' => "col-md-6 col-lg-6");
      }
      if(count($forms)==1){
        $class= array('container' => "col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-2",
                      'columns' => "col-md-12 col-lg-12");
      }

      $event_id = $request->event_id;
      //get last events
      $query = 'SELECT '.$this->fields.'
               FROM events
               WHERE status = 1
               LIMIT 7';
      $featuredEvents = DB::select($query);
      return view('buyStep_2', compact('class',
                                       'forms',
                                       'user',
                                       'event_id',
                                       'featuredEvents'));
    }

    // Buy data process Step 3 (call tbk)
    function buyData(Request $request){

      //Check and get user
      $user = Auth::user();
      if(!$user) return redirect('/site/login');

      //Create invoice
      $invoice = new Invoice;
      $invoice->user_id = $user->id;
      $invoice->event_id=$request->event_id;
      $invoice->total_price = $request->session()->get('total_price');
      $invoice->tickets_quantity = $request->session()->get('quantities');
      $invoice->discount = $request->session()->get('discount');
      $invoice->status=0;
      $invoice->save();

      //Clear session var
      $request->session()->forget('total_price');
      $request->session()->forget('quantities');
      $request->session()->forget('discount');

      //Run cicle
      foreach ($request->formdata as $data) {
        //Check if mail assistant already exist on db
        $assistant = Assistant::where('email',$data['email'])->first();
        if($assistant==null){
          //Create Assistant
          $assistant = new Assistant;
          $assistant->name = $data['name'];
          $assistant->email = $data['email'];
          $assistant->rut = $data['rut'];
          $assistant->gender = $data['gender'];
          $assistant->bday = $data['bday'];
          $assistant->extra_fields = array();
          $assistant->save();
        }

        //Insert or update json extra data
        try {
          foreach ($assistant->extra_fields as $field) {
            if(!array_key_exists($field['key'],$data['extra']) and $field['key']){
              $updated_array[] = $field;
            }
          }
          foreach($data['extra'] as $key => $value){
            $updated_array[]= array('key' => $key, 'value'=>$value);
          }
          $assistant->extra_fields = $updated_array;
          $assistant->save();
          unset($updated_array);
        } catch (\Exception $e) { /* do nothing */ }



        //assistant years old
        $_age = floor((time() - strtotime($assistant->bday)) / 31556926);
        //Get tickets type + categories tk
        $tType =  TicketsType::with('categoriesTk')
                              ->where('id',$data['id'])
                              ->get();
        //init var
        $category_tk_id = null;
        //For foreach categories
        foreach ($tType[0]->categoriesTk as $category){
          //Filter by gender
          if($category->gender == $assistant->gender){
            //Filter by range years old
            if( ($category->min_age <= $_age) && ($_age <= $category->max_age)){
              //Assign category to assistant
              $category_tk_id = $category->id;
              break;
            }
          }
        }//end foreach categories

        //Create ticket for Assistant
        $ticket = new Ticket;
        $ticket->ticket_type_id = $data['id'];
        $ticket->user_id = $user->id;
        $ticket->assistants_id = $assistant->id;
        $ticket->event_id = $request->event_id;
        $ticket->category_tk_id = $category_tk_id;
        $ticket->status = 0;
        $ticket->save();

        //Get tickets type + props
        $tType =  TicketsType::with('props')
                              ->where('id',$data['id'])
                              ->get();
        foreach ($tType[0]->props as $prop) {
          //Create Props (Check List)
          $check = new Checklist;
          $check->ticket_id = $ticket->id;
          $check->prop_id = $prop->id;
          $check->status  = 0;
          $check->save();
        }

        //Create QR code
        $route = '/qrcodes/qd-'.$ticket->id.'.png';
        $code = $ticket->id;
        $qr = QrCode::format('png')->size(500)->generate($code, '../public'.$route);
        $ticket->qrcode = $route;
        $ticket->save();


        //Create relation
        $invoiceTickets = new InvoiceTickets;
        $invoiceTickets->invoice_id = $invoice->id;
        $invoiceTickets->ticket_id = $ticket->id;
        $invoiceTickets->save();

      }//end foreach

      $tbk_data = array('total' => $invoice->total_price,
                        'session_id'=> $user->id.time(),
                        'buyOrder' => $invoice->id,
                        'returnUrl'=> route('wpResult'),
                        'finalUrl' => route('wpFinal'),
                        );

      //Call TransactionTbk
      $returnResponse = $this->transactionTbk($tbk_data);
      return view('formActionTbk', compact('returnResponse'));

    }

    //Helpers
    private function transactionTbk($tbk_data){
      if(!count($tbk_data)) return false;
      $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))
                                                ->getNormalTransaction();
      $initResult = $transaction->initTransaction($tbk_data['total'],
                                                  $tbk_data['buyOrder'],
                                                  $tbk_data['session_id'],
                                                  $tbk_data['returnUrl'],
                                                  $tbk_data['finalUrl']);
      return  array('formAction' => $initResult->url,
                    'tokenWs' => $initResult->token);

    }

    //Download ticket file
    public function downloadTicket(Request $request, $id){
      return false;
    }

    //Search Box
    public function searchEvent(Request $request){
      $fields = 'title,start_date,slug,img_square_panel,place';
      $query = 'SELECT '.$fields.'
                FROM events
                WHERE title like "%'.$request->keywords.'%"';
      $event = DB::select($query);
      return response()->json(json_encode($event));
    }

    //Validate Discount Code
    public function validateCode(Request $request){
      $discount = Discount::where('code',$request->code)->first();
      //Chek if exist
      if(!$discount){
        return response()->json("Este código no existe!");
      }
      //Check event
      if($discount->event_id!=$request->event_id){
        return response()->json("Este código no pertenece a este evento!");
      }
      //Check date
      if($discount->expiration < date('Y-m-d')){
        return response()->json("Este código ha expirado!");
      }

      $result = array('result' => 1,
                      'discount' => $discount->percent);
      return response()->json($result);
    }

    //Contact form
    public function contact(Request $request){

      //Get info to send email
      $dataemail = new \stdClass();

      $dataemail->name= $request->name;
      $dataemail->phone = $request->phone;
      $dataemail->email=$request->email;
      $dataemail->message=$request->message;
      $dataemail->subject = "Contacto web deporticket";

      try {
        Mail::to("info@deporticket.cl")
            ->cc($dataemail->email)
            ->send(new mailContact($dataemail));
        return response()->json(json_encode(1));
      } catch (\Exception $e) {
        echo $e; return;
        return response()->json($e);
      }

    }

    //Try function for testing
    public function try(Request $request){
      //get one assistant
      $assistant = Assistant::where('email',"christopher.troc@gmail.com")->first();

      $_age = floor((time() - strtotime($assistant->bday)) / 31556926);
      //Get tickets type + categories tk
      $tType =  TicketsType::with('categoriesTk')
                            ->where('id',5)
                            ->get();
      $_category = null;
      //For foreach categories
      foreach ($tType[0]->categoriesTk as $category){
          //Filter by gender
          if($category->gender == $assistant->gender){
            //Filter by age
            if( ($category->min_age <= $_age) && ($_age <= $category->max_age)){
              //Assign catefory to assistant
              echo "the category for assistant is: ".$category->name;
              break;
            }

          }

      } //end for
      exit;
    }//End function try


}//End class controller
