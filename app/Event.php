<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;


class Event extends Model
{
  protected $table = 'events';

  use Spatial;
  protected $spatial = ['coordinates'];

  //Tickets
  public function ticket(){
      return $this->hasMany('App\Ticket');
  }

  //Rules
  public function eventRules(){
      return $this->belongsToMany('App\Rule');
  }

  //Ticket Type
  public function eventTicketsType(){
    return $this->hasMany('App\TicketsType');
  }

}
