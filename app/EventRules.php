<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRules extends Model
{
    protected $table = 'events_rules';

    public function event(){
      return $this->belongsTo('App\Event', 'event_id');
    }

    public function rule(){
      return $this->belongsTo('App\Rule', 'rule_id');
    }
}
