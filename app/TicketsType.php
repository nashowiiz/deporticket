<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsType extends Model
{
    protected $table = 'tickets_type';

    public function event(){
      return $this->belongsTo('App\Event', 'event_id');
    }
    //props
    public function props(){
      return $this->belongsToMany('App\Prop','prop_tickets_type');
    }
    //categories tk
    public function categoriesTk(){
      return $this->belongsToMany('App\CategoriesTk','rel_tkt_cat');
    }
    //categories tk
    public function extraFields(){
      return $this->belongsToMany('App\ExtraFields','rel_tkt_field');
    }
}
