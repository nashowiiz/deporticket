<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    //Assistant
    public function assistant(){
      return $this->belongsTo('App\Assistant', 'assistants_id');
    }
    //User System
    public function user(){
      return $this->belongsTo('App\User', 'user_id');
    }
    //Event
    public function event(){
      return $this->belongsTo('App\Event', 'event_id');
    }

    //Ticket type
    public function ticketType(){
      return $this->belongsTo('App\TicketsType', 'ticket_type_id');
    }


}
