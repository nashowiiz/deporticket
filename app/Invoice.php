<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $casts = [
           'tickets_quantity' => 'array'
       ];

    public function invoiceTicket(){
        return $this->hasMany('App\InvoiceTickets');
    }
}
