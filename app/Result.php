<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Result extends Model
{
  protected   $fillable  = ['events_id',
                            'rider_name',
                            'distance',
                            'category',
                            'tmp_official',
                            'tmp_chip',
                            'lg_category',
                            'lg_general',
                            'lg_gender'];

  protected $table = 'results';
}
