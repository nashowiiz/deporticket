<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;

//Try route (for test)
Route::get('try','IndexController@try');
//Home (all events)
Route::get('home','IndexController@index')->name('home');
Route::get('/','IndexController@index')->name('index');
//Detail event (single event)
Route::get('event/{slug}', 'IndexController@event');
//Buy ticket (id_ticket) - Step 1
Route::get('buy/{slug}','IndexController@buyTickets');
//Buy Forms - Step 2
Route::post('buyForms','IndexController@buyForms');
//Process Buy data - Step 3
Route::post('buyData','IndexController@buyData');
Route::get('download/{name}','IndexController@downloadTicket')->name('download');
//webpay plus transbank
Route::post('/site/webpayResult','ResultController@webpayResult')->name('wpResult');
Route::post('/site/webpayFinal','ResultController@finalProcessTbk')->name('wpFinal');
// OAuth Routes facebook
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
//Auth-Site
Route::get('site/login','IndexController@displayLogin')->name('login_site');
Route::get('site/exit','Auth\LoginController@logout')->name('exit');
Route::post('site/starSession','Auth\LoginController@axiosLogin')->name('start_session');
Route::post('site/register','Auth\LoginController@axiosRegister')->name('register_site');
Route::post('site/updateUser','Auth\VerificationController@axiosUpdate')->name('update_site');
//Account panel
Route::get('site/Account','IndexController@myAccount')->name('account_url');
//Contact Form
Route::post('site/contact','IndexController@contact')->name('contact');
//Search
Route::post('site/search','IndexController@searchEvent')->name('search_url');
//Validate code discount route
Route::post('site/validateCode','IndexController@validateCode')->name('validate_code_url');
//Auth-default
Auth::routes();

/*Cors Routes for api
Route::group(['middleware' => 'cors'],function(){
  Route::post('site/updateUser','Auth\LoginController@axiosUpdate')->name('update_site');
});
*/

//Admin Voyager
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/export-assistant-list/{id}','Voyager\CheckinController@ExportAssistantList');
    Route::get('/QrCheckIn','Voyager\QrController@index');
    Route::post('/getDataReport','Voyager\CheckinController@getDataReport')->name('getDataReport');
    Route::post('/burnGetData','Voyager\QrController@burnGetData')->name('burnGetData');
    Route::post('/toggleBurnProp','Voyager\QrController@toggleBurnProp')->name('toggleBurnProp');

    Route::get('/mail/select/{id}','Voyager\MailingController@selectUsers')->name('emailSelectUsers');
    Route::post('/mail/sendEmailUsers', 'Voyager\MailingController@sendEmailUsers')->name('emailSendUsers');
    Route::post('/importAssistants','Voyager\AssistantController@importAssistants')->name('importAssistants');
    Route::post('/importResults','Voyager\ResultController@importResult')->name('importResult');
    //try function
    Route::get('/mail/try', 'Voyager\MailingController@try');

});
